import socket
import random


myPort = 1234
mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
mySocket.bind(('', myPort))


mySocket.listen(5)

random.seed()


try:
    while True:
        print("Waiting for connections")
        (recvSocket, address) = mySocket.accept()
        print("HTTP request received:")
        print(recvSocket.recv(2048))

        urls = [
            "https://www.google.com",
            "https://www.facebook.com",
            "https://www.youtube.com",
            "https://www.wikipedia.org",
            "https://www.amazon.com",
            "https://twitter.com",
            "https://www.instagram.com",
            "https://www.reddit.com",
            "https://www.linkedin.com",
            "https://www.netflix.com",
            "https://www.ebay.com",
        ]

        nextPage = str(random.randint(0, 10000))

        nextPage = random.choice([nextPage] + urls)
        nextUrl = "http://localhost:" + str(myPort) + "/" + nextPage

        htmlBody = "<h1>Perfect</h1>" + '<p>Next page: <a href="' \
           + nextPage + '">' "</a></p>"
        response = ("HTTP/1.1 307 Temporary Redirect\r\n" +
                    "Location: " + nextPage + "\r\n" +
                    "Content-Length: " + str(len(htmlBody)) + "\r\n" +
                    "\r\n" +
                    "<html><body>" + htmlBody + "</body></html>\r\n")
        print(nextPage)
        recvSocket.send(response.encode('utf-8'))
        recvSocket.close()

except KeyboardInterrupt:
    print("Closing binded socket")
    mySocket.close()
